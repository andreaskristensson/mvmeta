# mvmeta

Perl script for renaming / moving pictures and videos in a folder.

File meta information (date, time) and a tiny hash is used to construct new filename.

Example: `DSC_0001.JPG -> 2015-01-01T12-01-01-5c6ea79.JPG`

## Requires
### ExifTool
Install and make sure Perl can find the libs on your system.

* http://www.sno.phy.queensu.ca/~phil/exiftool/
* http://www.sno.phy.queensu.ca/~phil/exiftool/install.html#Unix

## Usage
```
mvmeta <directory containing jpg and mp4 files>

```