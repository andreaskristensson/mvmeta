#!/usr/bin/perl

use strict;
use warnings;

use Digest::MD5 'md5_hex';
#use Data::Dumper;
use File::Basename;
use File::Copy qw(move);
use Getopt::Long qw(:config posix_default);
use Path::Class;

# ExifTool; read, write, edit meta information.
# http://www.sno.phy.queensu.ca/~phil/exiftool/
use Image::ExifTool;


my $help_txt = "\nUsage: " . basename(__FILE__) . " [--dry-run] directory\n";


# Handle command line; consume options, handle arguments; need a directory.
my $dry_run = '';
GetOptions('dry-run' => \$dry_run);

#print Dumper \@ARGV;
my ($dir_str) = @ARGV;
if (not defined $dir_str) {
  print $help_txt;
  exit;
}

# Replace zero or more slash occurences at end with exactly one slash.
$dir_str =~ s!/*$!/!;

# Sanity check; make sure it's a directory.
if (! -d $dir_str) {
  print $help_txt;
  exit;
}

#
my $dir = dir($dir_str);
for my $f ($dir->children()) {

  # step over directories and any files not jpg or mp4.
  next if $f->is_dir;
  next if (!($f =~ /\.(jpg|nef|mp4)$/i));

  # exif
  my $exif = Image::ExifTool->new;
  $exif->ExtractInfo($f->stringify);

  # jpg datetime ok
  # mp4 datetime ok
  my $date = $exif->GetValue('CreateDate', 'PrintConv');

#  # jpg datetime ok
#  # mp4 datetime not ok; ?!
#  my $date = $exif->GetValue('DateTimeOriginal', 'PrintConv');

  # only rename if there's a valid date present; if there are
  # untouched files after the script is run those files can be
  # renamed manually; and the script could then be updated to
  # handle that case / file type.
  next unless defined $date;

  # construct new filename; date, time, hash (just in case).
  # 2014:11:16 14:12:32 -> 2014-11-16t14-12-32
  $date =~ tr/: /-t/;
  my $digest = substr(md5_hex($f->slurp), 0, 7);
  my($new_filename, $new_dir, $new_suffix) = fileparse($f->basename, qr/\.[^.]*/);
  $new_filename = lc "$date-$digest$new_suffix";
  
  # move / rename file.
  unless ($f->basename eq $new_filename) {
    if ($dry_run) {
      print "[dry-run] renamed ", $f->basename, " -> ", $dir_str, $new_filename, "\n";
    } else {
      move $f, $dir_str . $new_filename;
      print "renamed ", $f->basename, " -> ", $dir_str, $new_filename, "\n";
    }
  }
}

#
exit 0;
